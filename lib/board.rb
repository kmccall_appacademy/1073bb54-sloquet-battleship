class Board
  attr_reader :grid

  def initialize(grid = Board.default_grid)
    @grid = grid
  end

  def self.default_grid
    Array.new(10) {Array.new(10)}
  end

  #default ships are only one unit length
  def count
    ships = 0
    @grid.flatten.each {|space| ships += 1 if space == :s}
    ships
  end

  def empty?(position=false)
    return false if !position && self.count > 0
    return true if !position && self.count == 0
    @grid[position[0]][position[1]] == nil
  end

  def full?
    !@grid.flatten.include?(nil)
  end

  def [](pos)
    x, y = pos
    grid[x][y]
  end

  def []=(pos, val)
    x, y = pos
    grid[x][y] = val
  end

  def place_random_ship
    raise "error" if self.full?
    random_space = [rand(@grid.length), rand(@grid.length)]
    until empty?(random_space)
      random_space = [rand(@grid.length), rand(@grid.length)]
    end
    @grid[random_space[0]][random_space[1]] = :s
  end

  def won?
    self.count == 0
  end

end
